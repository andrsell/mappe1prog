
import hospital.Department;
import hospital.people.Employee;
import hospital.people.Patient;
import hospital.exception.RemoveException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * The type Department test.
 */
class DepartmentTest {


    /**
     * Remove patient.
     *
     * @throws RemoveException the remove exception
     */
    @Test
    void removePatient() throws RemoveException{
        Department emergency = new Department("akutten");
        Patient patient1 = new Patient("Andreas","Madsen", "10000");
        emergency.getPatients().add(patient1);


        int sizeBefore = emergency.getPatients().size();
        emergency.remove(patient1); // tries to remove patient
        int sizeAfter  = emergency.getPatients().size();


        //checks if sizeBefore and After are correct values
        assertEquals(1, sizeBefore);
        assertEquals(0, sizeAfter);

    }

    /**
     * Negative patient remove test.
     *
     * @throws RemoveException the remove exception
     */
    @Test
    void negativePatientRemoveTest() throws RemoveException {
        Department emergency = new Department("akutten");
        Patient patient1 = new Patient("Andreas","Madsen", "10000");
        emergency.getPatients().add(patient1);

        int sizeBefore = emergency.getPatients().size();
        emergency.remove(patient1); // tries to remove patient
        int sizeAfter  = emergency.getPatients().size();

        fail("This is a negative test, the test is supposed to fail");
        assertEquals(sizeBefore,sizeAfter);

    }

    /**
     * Remove employee.
     *
     * @throws RemoveException the remove exception
     */
    @Test
    void removeEmployee() throws RemoveException{
        Department emergency = new Department("akutten");
        Employee employee1 = new Employee("Andreas","Madsen", "10000");
        emergency.getEmployees().add(employee1);


        int sizeBefore = emergency.getEmployees().size();
        emergency.remove(employee1); // tries to remove patient
        int sizeAfter  = emergency.getEmployees().size();


//checks if sizeBefore and After are correct values
        assertEquals(1, sizeBefore);
        assertEquals(0, sizeAfter);

    }

    /**
     * Negative employee remove test.
     *
     * @throws RemoveException the remove exception
     */
    @Test
    void negativeEmployeeRemoveTest() throws RemoveException {
        Department emergency = new Department("akutten");
        Employee employee1 = new Employee("Andreas","Madsen", "10000");
        emergency.getEmployees().add(employee1);

        int sizeBefore = emergency.getEmployees().size();
        emergency.remove(employee1); // tries to remove patient
        int sizeAfter  = emergency.getEmployees().size();



        fail("This is a negative test, the test is supposed to fail");
        assertEquals(sizeBefore,sizeAfter);

    }
}