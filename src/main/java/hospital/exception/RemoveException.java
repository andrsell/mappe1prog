package hospital.exception;

/**
 * The type Remove exception.
 */
public class RemoveException extends Exception{
    /**
     * Instantiates a new Remove exception.
     *
     * @param removeException the remove exception
     */
    public RemoveException(String removeException) {
        super(removeException);
    }
}
