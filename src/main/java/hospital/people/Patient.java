package hospital.people;

/**
 * The type hospital.people.Patient.
 */
public class Patient extends Person{
    private String diagnosis = "";

    /**
     * Instantiates a new hospital.people.Patient.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Patient(String firstName, String lastName, String socialSecurityNumber) {
        super();
    }

    /**
     * Gets diagnosis.
     *
     * @return the diagnosis
     */
    public String getDiagnosis() {
        return diagnosis;
    }

    @Override
    public String toString() {
        return "hospital.people.Patient{" +
                "diagnosis='" + diagnosis + '\'' +
                '}';
    }

    /**
     * Set diagnosis.
     *
     * @param string the string
     */
    public void setDiagnosis(String string){
        diagnosis = string;
    }
}
