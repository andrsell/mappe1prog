package hospital;

import hospital.exception.RemoveException;
import hospital.people.Employee;
import hospital.people.Patient;
import hospital.people.Person;

import java.util.ArrayList;
import java.util.List;

/**
 * The type hospital.Department.
 */
public class Department {
    private String departmentName;

    /**
     * Instantiates a new hospital.Department.
     *
     * @param departmentName the department name
     */
    public Department(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * The hospital.people.Employee array list.
     */
  private   ArrayList<Employee> employeeArrayList = new ArrayList<>();
    /**
     * The hospital.people.Patient array list.
     */
   private   ArrayList<Patient> patientArrayList = new ArrayList<>();

    /**
     * Gets department name.
     *
     * @return the department name
     */
    public String getDepartmentName() {
        return departmentName;
    }

    /**
     * Sets department name.
     *
     * @param departmentName the department name
     */
    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    /**
     * Gets employees.
     *
     * @return the employees
     */
    public List<Employee> getEmployees() {
        return employeeArrayList;
    }

    /**
     * Add employee.
     *
     * @param employee the employee
     */
    public void addEmployee(Employee employee) {
        employeeArrayList.add(employee);
    }

    /**
     * Gets patients.
     *
     * @return the patients
     */
    public List<Patient> getPatients() {
        return patientArrayList;
    }

    /**
     * Add patient.
     *
     * @param patient the patient
     */
    public void addPatient(Patient patient) {
        patientArrayList.add(patient);
    }


    /**
     * Remove.
     *
     * @param person the person
     * @throws RemoveException the remove exception
     */
    public void remove(Person person) throws RemoveException {
        if (person instanceof Employee & employeeArrayList.contains(person)) {
            employeeArrayList.remove(person);
        } else if (person instanceof Patient & patientArrayList.contains(person)) {
            patientArrayList.remove(person);
        } else throw new RemoveException("removeException");
    }

    @Override
    public String toString() {
        return "hospital.Department{" +
                "departmentName='" + departmentName + '\'' +
                '}';
    }
}
