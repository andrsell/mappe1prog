package hospital;

/**
 * The interface Diagnosable.
 */
public interface Diagnosable {

    /**
     * Sets diagnosis.
     *
     * @param string the string
     */
    void setDiagnosis(String string);
}
