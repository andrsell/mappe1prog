package hospital.healthPersonal.doctor;

import hospital.people.Patient;

public class GeneralPractitioner extends Doctor {

    public GeneralPractitioner(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

    @Override
    public void setDiagnosis(Patient patient, String string) {
        super.setDiagnosis(patient, string);
    }
}
