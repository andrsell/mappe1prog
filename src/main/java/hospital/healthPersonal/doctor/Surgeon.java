package hospital.healthPersonal.doctor;

import hospital.people.Patient;

/**
 * The type Surgeon.
 */
public class Surgeon extends Doctor{
    /**
     * Instantiates a new Surgeon.
     *
     * @param firstName            the first name
     * @param lastName             the last name
     * @param socialSecurityNumber the social security number
     */
    public Surgeon(String firstName, String lastName, String socialSecurityNumber) {
        super(firstName, lastName, socialSecurityNumber);
    }

@Override
    public void setDiagnosis(Patient patient, String string){
        super.setDiagnosis(patient, string);
}

}
