package hospital;

import java.util.ArrayList;
import java.util.List;

/**
 * The type hospital.Hospital.
 */
public class Hospital {
    private final String hospitalName;
    /**
     * The hospital.Department array list.
     */
    ArrayList<Department> departmentArrayList = new ArrayList<>();

    /**
     * Instantiates a new hospital.Hospital.
     *
     * @param hospitalName the hospital name
     */
    public Hospital(String hospitalName) {
        this.hospitalName = hospitalName;
    }

    /**
     * Gets hospital name.
     *
     * @return the hospital name
     */
    public String getHospitalName() {
        return hospitalName;
    }

    /**
     * Gets departments.
     *
     * @return the departments
     */
    public List<Department> getDepartments() {
        return departmentArrayList;
    }

    /**
     * Add department.
     *
     * @param department the department
     */
    public void addDepartment(Department department) {
        departmentArrayList.add(department);
    }

    @Override
    public String toString() {
        return "hospital.Hospital{" + "\n" +
                "hospitalName= " + hospitalName + "\n" +
                '}';
    }


}
