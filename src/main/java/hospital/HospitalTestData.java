package hospital;

import hospital.healthPersonal.Nurse;
import hospital.healthPersonal.doctor.GeneralPractitioner;
import hospital.healthPersonal.doctor.Surgeon;
import hospital.people.Employee;
import hospital.people.Patient;

import java.util.Random;

public final class HospitalTestData {



    private HospitalTestData() {
    }
    /**
     * @param hospital
     */
    public static void fillRegisterWithTestData(final Hospital hospital) {
        // Add some departments
        Department emergency = new Department("Akutten");
        emergency.getEmployees().add(new Employee("Odd Even", "Primtallet", "12345"));
        emergency.getEmployees().add(new Employee("Huppasahn", "DelFinito", "54321"));
        emergency.getEmployees().add(new Employee("Rigmor", "Mortis", "95467"));
        emergency.getEmployees().add(new GeneralPractitioner("Inco", "Gnito", "71495"));
        emergency.getEmployees().add(new Surgeon("Inco", "Gnito", "75940"));
        emergency.getEmployees().add(new Nurse("Nina", "Teknologi", "29993"));
        emergency.getEmployees().add(new Nurse("Ove", "Ralt", "72648"));
        emergency.getPatients().add(new Patient("Inga", "Lykke", "64584"));
        emergency.getPatients().add(new Patient("Ulrik", "Smål", "00194"));
        hospital.getDepartments().add(emergency);
        Department childrenPolyclinic = new Department("Barn poliklinikk");
        childrenPolyclinic.getEmployees().add(new Employee("Salti", "Kaffen", "47970"));
        childrenPolyclinic.getEmployees().add(new Employee("Nidel V.", "Elvefølger", "33959"));
        childrenPolyclinic.getEmployees().add(new Employee("Anton", "Nym", "78963"));
        childrenPolyclinic.getEmployees().add(new GeneralPractitioner("Gene", "Sis", "37896"));
        childrenPolyclinic.getEmployees().add(new Surgeon("Nanna", "Na", "44173"));
        childrenPolyclinic.getEmployees().add(new Nurse("Nora", "Toriet", "96077"));
        childrenPolyclinic.getPatients().add(new Patient("Hans", "Omvar", "28449"));
        childrenPolyclinic.getPatients().add(new Patient("Laila", "La", "16839"));
        childrenPolyclinic.getPatients().add(new Patient("Jøran", "Drebli", "05997"));
        hospital.getDepartments().add(childrenPolyclinic);

    }

}
