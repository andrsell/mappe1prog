package hospital;

import hospital.exception.RemoveException;
import hospital.people.Employee;

/**
 * The type hospital.Hospital client.
 */
public class HospitalClient {
    private final Hospital hospital;

    /**
     * Instantiates a new hospital.Hospital client.
     *
     * @param hospital the hospital
     */
    public HospitalClient(Hospital hospital) {
        this.hospital = hospital;
    }

    /**
     * The entry point of application.
     *
     * @param args the input arguments
     */
    public static void main(String[] args) {

HospitalClient hospitalClient = new HospitalClient(new Hospital("st. olavs")); // creates a new hospital client with a hospital
HospitalTestData.fillRegisterWithTestData(hospitalClient.hospital); // fills the hospital with test data
try {
    Employee e = new Employee("andreas", "sellesbakk", "12344444"); // creates a new employee
    hospitalClient.hospital.getDepartments().get(0).remove(hospitalClient.hospital.getDepartments().get(0).getEmployees().get(0)); //removes a employee already in the list
hospitalClient.hospital.getDepartments().get(0).remove(e); // tries to remove an employee that is not in the list

}
catch (RemoveException re){
    System.err.println("an error has occurred: " + re.getMessage());
}

    }
}
